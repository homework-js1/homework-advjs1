class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    get employeeName() {
        return this.name;
    }
    get employeeAge() {
        return this.age;
    }
    get employeeSalary() {
        return this.salary;
    }

    set employeeName(employeeName) {
        this.name = employeeName;
    }
    set employeeAge(employeeAge) {
        this.age = employeeAge;
    }
    set employeeSalary(employeeSalary) {
        this.salary = employeeSalary;
    }

}

class Programmer extends Employee {
    constructor(name, age, salary) {
        super(name, age, salary);
        this.lang = prompt('Enter lang (c, c#...)');
    }
    getEmployeeSalary() {
      this.salary = this.salary * 3;
        return this;
    }
}

const emp = new Employee(prompt('Enter name'), prompt('Enter age'),  prompt('Enter salary'));
console.log(emp);

const pr  = new Programmer( prompt('Enter name'), prompt('Enter age'), emp.employeeSalary);
pr.getEmployeeSalary()
console.log(pr);
const pr2  = new Programmer( prompt('Enter name'), prompt('Enter age'), emp.employeeSalary);
pr2.getEmployeeSalary()
console.log(pr2);
const pr3  = new Programmer( prompt('Enter name'), prompt('Enter age'), emp.employeeSalary);
pr3.getEmployeeSalary()
console.log(pr3);